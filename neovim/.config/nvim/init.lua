-- ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗
-- ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║
-- ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║
-- ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║
-- ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║
-- ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝

-- █▓▒░ Core
require("core.plugins")
require("core.options")
require("core.keymap")
require("core.colorscheme")

-- █▓▒░ Plugins
require("plugins.indent-blankline")
require("plugins.mason")
require("plugins.comment")
require("plugins.nvim-cmp")
require("plugins.lspsaga")
require("plugins.lspconfig")
require("plugins.treesitter")
require("plugins.nvim-tree")
require("plugins.telescope")
require("plugins.bufferline")
require("plugins.gitsigns")
require("plugins.null-ls")
require("plugins.lualine")
require("plugins.autopairs")
