local opts = { noremap = true, silent = true }
local keymap = vim.api.nvim_set_keymap

-- █▓▒░ Leader
vim.g.mapleader = " "
keymap("", "<Space>", "<Nop>", opts)

-- █▓▒░ Windows
keymap("n", "<leader>sv", "<C-w>v", opts) -- Split window vertically
keymap("n", "<leader>sh", "<C-w>s", opts) -- Split window horizontally
keymap("n", "<leader>se", "<C-w>=", opts) -- Make split windows equal width
keymap("n", "<leader>sx", ":close<CR>", opts) -- Close current split window

--  █▓▒░ Tabs
keymap("n", "<leader>to", ":tabnew<CR>", opts) -- Open a new tab
keymap("n", "<leader>tx", ":tabclose<CR>", opts) -- Close current tab
keymap("n", "<leader>tn", ":tabn<CR>", opts) -- Go to the next tab
keymap("n", "<leader>tp", ":tabp<CR>", opts) -- Go to the previous tab

--  █▓▒░ NVim Tree
keymap("n", "<leader>e", ":NvimTreeToggle<CR>", opts) -- Toggle file explorer

-- Telescope --
keymap("n", "<leader>ff", "<cmd>Telescope find_files<cr>", opts)
keymap("n", "<leader>fs", "<cmd>Telescope live_grep<cr>", opts)
keymap("n", "<leader>fc", "<cmd>Telescope grep_string<cr>", opts)
keymap("n", "<leader>fb", "<cmd>Telescope buffers<cr>", opts)
keymap("n", "<leader>fh", "<cmd>Telescope help_tags<cr>", opts)
