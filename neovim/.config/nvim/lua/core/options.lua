local opt = vim.opt

-- █▓▒░ Line numbers
opt.relativenumber = true
opt.number = true

-- █▓▒░ Tabs & Indentation
opt.tabstop = 2
opt.shiftwidth = 2
opt.expandtab = true
opt.autoindent = true

-- █▓▒░ Line Wrapping
opt.wrap = false

-- █▓▒░ Search
opt.ignorecase = true
opt.smartcase = true
opt.hlsearch = true

-- █▓▒░ Cursor Line
opt.cursorline = true

-- █▓▒░ Appearance
opt.termguicolors = true
opt.signcolumn = "yes"
opt.showmode = false

-- █▓▒░ Backspace
opt.backspace = "indent,eol,start"

-- █▓▒░ Clipboard
opt.clipboard:append("unnamedplus")

-- █▓▒░ Split Windows
opt.splitright = true
opt.splitbelow = true

-- █▓▒░ Keywords
opt.iskeyword:append("-")
